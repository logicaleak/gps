import datetime
import time
import MySQLdb
import os
import re
import traceback

DB_IP = "192.168.1.100"
DB_USER = "raspery"
DB_PASS = "1234"
TABLE_NAME = "gps_konum"
DB_NAME = "plakadb"
WAIT_S = 3

query = """
    update {tableName} set enlem={lat}, boylam={lon}, update_time=NOW()
"""

class SQL():
    conn = None

    def __init__(self):
        self.connect()
    
    def connect(self):
	while True:
    	    try:
                self.conn = MySQLdb.connect(DB_IP, DB_USER, DB_PASS, DB_NAME)
	        print "reconnected"
	        break
	    except:
	        continue

    def updateDatabase(self, lat, lon):
	formattedQuery = query.format(tableName=TABLE_NAME, lat=lat, lon=lon)
        print "inupdate", formattedQuery
        try:
      	    cursor = self.conn.cursor()
            cursor.execute(formattedQuery)
            self.conn.commit()
            print "done", formattedQuery
        except Exception as e:
            print traceback.format_exc()
            print "reconnecting"
	    self.connect()


def gps_collect_function():
    sql = SQL()
    while True:
        try:
            with open('/dev/ttyUSB0', 'r') as f: 
                while True:
                    line = f.readline()
                    splittedText = line.split(",")

                    if splittedText[0] == "$GPRMC":
                        latitudeUnfixed = splittedText[3]
                        longtitudeUnfixed = splittedText[5]
                        
                        #Fix latitudeUnfixed
                        fMinute = re.search("\d{2}\.\d+", latitudeUnfixed)
                        minutePart = fMinute.group(0)
                        fDegree = latitudeUnfixed.split(minutePart)[0]
                        fixedLat = float(minutePart) / 60 + float(fDegree)
			print fixedLat
                        
                        #Fix long
                        fMinute = re.search("\d{2}\.\d+", longtitudeUnfixed)
                        minutePart = fMinute.group(0)
                        fDegree = longtitudeUnfixed.split(minutePart)[0]
                        fixedLon = float(minutePart) / 60 + float(fDegree)
                        
                        sql.updateDatabase(fixedLat, fixedLon)
		        time.sleep(WAIT_S)
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
	    print traceback.format_exc()
            continue
            
           

def main():
    gps_collect_function()
    

main()
